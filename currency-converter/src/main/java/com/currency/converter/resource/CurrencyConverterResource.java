package com.currency.converter.resource;

import com.currency.converter.dto.request.CurrencyConversionRequest;
import com.currency.converter.dto.response.CurrencyResponseModel;
import com.currency.converter.service.CurrencyConversionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@org.springframework.web.bind.annotation.RestController
@CrossOrigin("*")
public class CurrencyConverterResource {

    @Autowired
    private CurrencyConversionService currencyConversionService;

    @GetMapping("/currency-converter")
    public CurrencyResponseModel convert(@RequestParam String sourceCurrency, @RequestParam String targetCurrency, @RequestParam Double sourceAmount) {

        CurrencyConversionRequest currencyConversionRequest = new CurrencyConversionRequest(sourceCurrency, targetCurrency, sourceAmount);
        String targetAmount = currencyConversionService.convertCurrency(currencyConversionRequest);
        return new CurrencyResponseModel(targetAmount);
    }
}
