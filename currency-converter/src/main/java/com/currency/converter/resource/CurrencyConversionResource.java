package com.currency.converter.resource;

import com.currency.converter.dto.request.CurrencyConversionRequest;
import com.currency.converter.service.CurrencyConversionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/api")
public class CurrencyConversionResource {

    @Autowired
    private CurrencyConversionService currencyConversionService;

    @Value("${currency.types}")
    public String currencyTypes;

    @GetMapping("/currency-conversion")
    public String showConverterPage(Model model) {
        String[] currencies = currencyTypes.split(",");
        model.addAttribute("currencies", currencies);
        return "currency-converter";
    }

    @PostMapping("/currency-converter")
    public String convertCurrency(@ModelAttribute CurrencyConversionRequest currencyRequest,
                                  Model model) {
        String targetAmount = currencyConversionService.convertCurrency(currencyRequest);
        model.addAttribute("targetAmount", targetAmount);
        String selectedTargetCurrency = currencyRequest.getTargetCurrency();
        String selectedSourceCurrency = currencyRequest.getSourceCurrency();
        String amount = String.valueOf(currencyRequest.getSourceAmount());
        String[] currencies = currencyTypes.split(",");
        model.addAttribute("currencies", currencies);
        model.addAttribute("selectedTargetCurrency", selectedTargetCurrency);
        model.addAttribute("selectedSourceCurrency", selectedSourceCurrency);
        model.addAttribute("amount", amount);

        return "currency-converter";
    }

}
