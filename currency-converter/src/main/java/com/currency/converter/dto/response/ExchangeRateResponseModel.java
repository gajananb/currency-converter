package com.currency.converter.dto.response;

import com.fasterxml.jackson.annotation.JsonAnySetter;

import java.util.HashMap;
import java.util.Map;

public class ExchangeRateResponseModel {

    private String date;
    private Map<String, Double> rates = new HashMap<>();

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Map<String, Double> getRates() {
        return rates;
    }

    @JsonAnySetter
    public void setRates(String currency, Double rate) {
        rates.put(currency, rate);
    }
}
