package com.currency.converter.dto.response;

public class CurrencyResponseModel {

    private String amount;

    public CurrencyResponseModel(String amount) {
        this.amount = amount;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
