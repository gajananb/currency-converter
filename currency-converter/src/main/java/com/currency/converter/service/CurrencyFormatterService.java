package com.currency.converter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;

@Service
public class CurrencyFormatterService {

    @Autowired
    private Environment environment;
    public String format(double amount, String currencyCode) {
        DecimalFormat decimalFormat;
        int decimals = getDecimalsForCurrency(currencyCode);
        String pattern;
        switch (decimals) {
            case 0:
                pattern = "#,##0";
                break;
            case 1:
                pattern = "#,##0.0";
                break;
            default:
                pattern = "#,##0.00";
                break;
        }
        decimalFormat = new DecimalFormat(pattern);
        return decimalFormat.format(amount);
    }


    private int getDecimalsForCurrency(String currencyCode) {
        String propertyName = "currency.decimals." + currencyCode.toLowerCase();
        String values = environment.getProperty(propertyName, "2");

        if (!values.isEmpty() && values != null) {
            return Integer.parseInt(values);
        } else {
            return 2;
        }
    }
}
