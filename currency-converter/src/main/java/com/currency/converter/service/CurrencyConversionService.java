package com.currency.converter.service;

import com.currency.converter.dto.request.CurrencyConversionRequest;
import com.currency.converter.dto.response.ExchangeRateResponseModel;
import com.currency.converter.feign.CurrencyApiFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CurrencyConversionService {

    @Autowired
    private CurrencyApiFeignClient currencyApiFeignClient;
    @Autowired
    private CurrencyFormatterService currencyFormatterService;


    public String convertCurrency(CurrencyConversionRequest currencyConversionRequest) {
        ExchangeRateResponseModel exchangeRateResponse = currencyApiFeignClient.getExchangeRate(
                currencyConversionRequest.getSourceCurrency().toLowerCase(),
                currencyConversionRequest.getTargetCurrency().toLowerCase()
        );

        if (exchangeRateResponse != null) {
            double exchangeRate = exchangeRateResponse.getRates().get(currencyConversionRequest.getTargetCurrency().toLowerCase());
            return currencyFormatterService.format(currencyConversionRequest.getSourceAmount() * exchangeRate, currencyConversionRequest.getTargetCurrency().toLowerCase());
        } else {
            throw new RuntimeException("Failed to fetch exchange rate from API");
        }
    }


}

