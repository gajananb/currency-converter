package com.currency.converter.feign;


import com.currency.converter.dto.response.ExchangeRateResponseModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "currency-api", url = "${currency.api.url}")
public interface CurrencyApiFeignClient {

    @GetMapping("/{sourceCurrency}/{targetCurrency}.json")
    ExchangeRateResponseModel getExchangeRate(@PathVariable("sourceCurrency") String sourceCurrency,
                                              @PathVariable("targetCurrency") String targetCurrency);
}
