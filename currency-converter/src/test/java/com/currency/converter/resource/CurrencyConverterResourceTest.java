package com.currency.converter.resource;

import com.currency.converter.dto.response.CurrencyResponseModel;
import com.currency.converter.service.CurrencyConversionService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class CurrencyConverterResourceTest {

    @InjectMocks
    private CurrencyConverterResource currencyConverterResource;

    @Mock
    private CurrencyConversionService currencyConversionService;

    @Before
    public void setUp() {

    }

    @Test
    public void testConvert() {

        String sourceCurrency = "USD";
        String targetCurrency = "EUR";
        Double sourceAmount = 100.0;
        String expectedTargetAmount = "90.0";

        when(currencyConversionService.convertCurrency(any())).thenReturn(expectedTargetAmount);
        CurrencyResponseModel responseModel = currencyConverterResource.convert(sourceCurrency, targetCurrency, sourceAmount);

        assertEquals(expectedTargetAmount, responseModel.getAmount());
    }
}
