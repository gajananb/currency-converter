package com.currency.converter.resource;

import com.currency.converter.dto.request.CurrencyConversionRequest;
import com.currency.converter.service.CurrencyConversionService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.ui.Model;

import static org.mockito.Mockito.*;
import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class CurrencyConversionResourceTest {

    @InjectMocks
    private CurrencyConversionResource currencyConversionResource;

    @Mock
    private CurrencyConversionService currencyConversionService;

    @Mock
    private Model model;

    @Before
    public void setUp() {

        currencyConversionResource.currencyTypes = "USD,EUR,INR,JPY";
    }

    @Test
    public void testShowConverterPage() {
        // When
        String viewName = currencyConversionResource.showConverterPage(model);

        // Then
        assertEquals("currency-converter", viewName);
        verify(model).addAttribute("currencies", new String[]{"USD", "EUR", "INR", "JPY"});
    }

    @Test
    public void testConvertCurrency() {

        CurrencyConversionRequest currencyRequest = new CurrencyConversionRequest("USD", "EUR", 100.0);
        String expectedTargetAmount = "90.0";
        when(currencyConversionService.convertCurrency(currencyRequest)).thenReturn(expectedTargetAmount);

        String viewName = currencyConversionResource.convertCurrency(currencyRequest, model);
        assertEquals("currency-converter", viewName);
        verify(model).addAttribute("targetAmount", expectedTargetAmount);
        verify(model).addAttribute("selectedTargetCurrency", "EUR");
        verify(model).addAttribute("selectedSourceCurrency", "USD");
        verify(model).addAttribute("amount", "100.0");
        verify(model).addAttribute("currencies", new String[]{"USD", "EUR", "INR", "JPY"});
    }
}
