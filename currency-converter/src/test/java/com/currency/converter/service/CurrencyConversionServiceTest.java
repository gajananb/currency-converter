package com.currency.converter.service;

import com.currency.converter.dto.request.CurrencyConversionRequest;
import com.currency.converter.dto.response.ExchangeRateResponseModel;
import com.currency.converter.feign.CurrencyApiFeignClient;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
public class CurrencyConversionServiceTest {

    @Mock
    private CurrencyApiFeignClient currencyApiFeignClient;

    @Mock
    private CurrencyFormatterService currencyFormatterService;

    @InjectMocks
    private CurrencyConversionService currencyConversionService;

    @Test
    public void testConvertCurrency() {
        // Given
        CurrencyConversionRequest request = new CurrencyConversionRequest("USD", "EUR", 100.0);
        ExchangeRateResponseModel responseModel = new ExchangeRateResponseModel();
        responseModel.setRates("EUR", 0.9); // Assuming 1 USD = 0.9 EUR
        when(currencyApiFeignClient.getExchangeRate("usd", "eur")).thenReturn(responseModel);
        when(currencyFormatterService.format(90.0, "eur")).thenReturn("90.00 EUR");

        // When
        String result = currencyConversionService.convertCurrency(request);

        // Then
        assertEquals("90.00 EUR", result);
    }
}
