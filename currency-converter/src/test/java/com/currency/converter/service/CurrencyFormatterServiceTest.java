package com.currency.converter.service;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
public class CurrencyFormatterServiceTest {

    @Mock
    private Environment environment;

    @InjectMocks
    private CurrencyFormatterService currencyFormatterService;

    @BeforeEach
    public void setup() {
        currencyFormatterService = new CurrencyFormatterService();
        ReflectionTestUtils.setField(currencyFormatterService, "environment", environment);
    }


    @Test
    public void testFormat() {
        double amount = 1234.5678;
        String currencyCode = "USD";
        when(environment.getProperty("currency.decimals.usd", "2")).thenReturn("2");

        String formattedAmount = currencyFormatterService.format(amount, currencyCode);
        assertEquals("1,234.57", formattedAmount);
    }

    @Test
    public void testFormatWithDifferentCurrency() {

        double amount = 1234.5678;
        String currencyCode = "JPY";
        when(environment.getProperty("currency.decimals.jpy", "2")).thenReturn("0");

        String formattedAmount = currencyFormatterService.format(amount, currencyCode);
        assertEquals("1,235", formattedAmount);
    }

    @Test
    public void testFormatWithUnknownCurrency() {

        double amount = 1234.5678;
        String currencyCode = "XYZ";
        when(environment.getProperty("currency.decimals.xyz", "2")).thenReturn("2");

        String formattedAmount = currencyFormatterService.format(amount, currencyCode);
        assertEquals("1,234.57", formattedAmount);
    }
}
