import { useState } from "react";
import "./App.css";

function App() {
  const [model, setModel] = useState({});

  let currencies = [
    { value: "INR", text: "Indian Rupee" },
    { value: "USD", text: "US Dollars" },
    { value: "EUR", text: "Euro" },
    { value: "JPY", text: "Japanese Yen" },
  ];

  const [sourceCurrencyError, setSourceCurrencyError] = useState("");
  const [targetCurrencyError, setTargetCurrencyError] = useState("");
  const [onConvertCurrenecyError, setConvertCurrencyError] = useState("");

  const onSourceCurrencyChanged = (e) => {
    const value = e.target.value;

    if (!value || value === "Select Currency") {
      setSourceCurrencyError("Source Currency is required.");
    } else {
      setSourceCurrencyError("");
    }

    let targetCurrencies = currencies.filter((d) => d.value !== value);

    setTargetCurrencies(targetCurrencies);

    setModel({ ...model, SourceCurrency: value });
  };

  const onTargetCurrencyChanged = (e) => {
    const value = e.target.value;

    if (!value) {
      setTargetCurrencyError("Target Currency is required.");
    } else {
      setTargetCurrencyError("");
    }

    setModel({ ...model, TargetCurrency: value });
  };

  const handleSourceAmount = (e) => {
    const value = e.target.value;
    setModel({ ...model, SourceAmmount: value });
  };

  const convertCurrency = (e) => {
    if (typeof model.SourceCurrency != 'undefined' && typeof model.TargetCurrency != 'undefined' && typeof model.SourceAmmount != 'undefined') {
      fetch(`http://localhost:9090/currency-converter?sourceCurrency=${model.SourceCurrency.toLowerCase()}&targetCurrency=${model.TargetCurrency.toLowerCase()}&sourceAmount=${model.SourceAmmount}`)
        .then((response) => response.json())
        .then((json) => {
          var amount = json.amount;
          setModel({ ...model, TargetAmount: amount });
        })
        .catch((error) => console.error(error));
    } else {
      setConvertCurrencyError("Please select source currency or target currency or source amount");
    }
  };
  const [targetCurrencies, setTargetCurrencies] = useState([]);

  return (
    <div className="container mt-3">
      <h3>Currency Converter</h3>
      <div className="card mt-3">
        <div className="card-body">
          <div className="row">
            <div className="col-12 col-sm-6 mb-3">
              <label htmlFor="sourceCurrency" className="form-label">
                Source currency <span className="text-danger">*</span>
              </label>
              <div className="input-group mb-3">
                <span className="input-group-text" id="basic-addon1">
                  <select
                    className="form-select"
                    aria-label="Source currency"
                    id="sourceCurrency"
                    onChange={onSourceCurrencyChanged}
                  >
                    <option>Select Currency</option>
                    {currencies.map((d) => (
                      <option key={d.value} value={d.value}>
                        {d.text}
                      </option>
                    ))}
                  </select>
                </span>
                <input
                  type="text"
                  className="form-control"
                  id="sourceAmount"
                  placeholder="Source Amount"
                  value={model.SourceAmount}
                  onChange={handleSourceAmount}
                ></input>
              </div>
              <p className="text-danger">{sourceCurrencyError}</p>
            </div>
            <div className="col-12 col-sm-6 mb-3">
              <label htmlFor="targetCurrency" className="form-label">
                Target currency <span className="text-danger">*</span>
              </label>
              <div className="input-group mb-3">
                <span className="input-group-text" id="basic-addon1">
                  <select
                    className="form-select"
                    aria-label="Target currency"
                    id="targetCurrency"
                    onChange={onTargetCurrencyChanged}
                  >
                    <option>Select Currency</option>
                    {targetCurrencies.map((d) => (
                      <option key={d.value} value={d.value}>
                        {d.text}
                      </option>
                    ))}
                  </select>
                </span>
                <label
                  className="form-control"
                  id="targetAmount"
                >{model.TargetAmount}</label>
              </div>
              <p className="text-danger">{targetCurrencyError}</p>
            </div>
          </div>
          <div className="row">
            <div>
              <button onClick={convertCurrency} className="btn btn-primary">
                Convert
              </button>
              <p className="text-danger">{onConvertCurrenecyError}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
